from PyQt5 import QtWidgets
from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import pyqtSlot
import sys
from PyQt5.QtWidgets import (QApplication, QWidget, QRadioButton,QPushButton, QLabel, QLineEdit, QGridLayout, QMessageBox,QInputDialog,QVBoxLayout)
from Questions import *


class profanity(Exception):
    def __init__(self,a):
        Exception.__init__(self)
        self.a=a

class main_window(QMainWindow):
    def __init__(self):
        super(main_window,self).__init__()
        self.dict_bonus={}
        self.bonusQuestion=False
        self.score=0
        self.selectGoodAnswer=[]
        self.words=[]
        self.main_ui()


    def main_ui(self):
        self.setGeometry(400,150,1200,800)
        self.setWindowTitle("QUIZ PCAP")
        self.setWindowIcon(QtGui.QIcon('logo.png'))


        self.titleText=QLabel('Witaj w QUIZ o tematyce PCAP!',self)
        self.titleText.move(450,50)
        self.titleText.setFont(QFont('Times',20))
        self.titleText.adjustSize()



        self.start_button=QPushButton("Rozpocznij QUIZ",self)
        self.start_button.resize(150,35)
        self.start_button.move(530,300)
        self.start_button.setToolTip("Rozpoczyna QUIZ")
        self.start_button.clicked.connect(self.start_clicked)


        self.how_button=QPushButton("Zasady",self)
        self.how_button.resize(150,35)
        self.how_button.setToolTip("Wyświetla zasady QUIZU")
        self.how_button.move(530,350)
        self.how_button.clicked.connect(self.how_clicked)

        self.addQuestion=QPushButton("Dodaj pytanie",self)
        self.addQuestion.resize(150,35)
        self.addQuestion.setToolTip("Dodaje pytanie ")
        self.addQuestion.move(530,400)
        self.addQuestion.clicked.connect(self.addQuestion_clicked)


        self.exit_button=QPushButton("Wyjdź",self)
        self.exit_button.resize(150,35)
        self.exit_button.move(530,450)
        self.exit_button.setToolTip("Wyłącza aplikacje")
        self.exit_button.clicked.connect(self.exit_clicked)

    def start_clicked(self):
        self.forbidden_words=open("wulgaryzmy.txt","r")
        self.words=self.forbidden_words.read().split(";")
        self.forbidden_words.close()
        try:
            self.UserText,self.ok=QInputDialog.getText(self,'QUIZ PCAP','Podaj nick:')
            self.UserText_check=self.UserText.lower()
            for i in self.words:
                if i==self.UserText_check:
                    raise profanity("Profanity detected!\nTry again")
            if self.bonusQuestion==False:
                if self.ok:
                    self.hide()
                    self.open_window=quiz_windowQ1(self.UserText,self.score)
                    self.open_window.show()
            else:
                if self.ok:
                    self.hide()
                    self.open_window=bonus_question(self.UserText,self.dict_bonus,self.score,self.selectGoodAnswer)
                    self.open_window.show()
        except profanity as profanity_error:
            QMessageBox.about(self,"Profanity",profanity_error.a)


    def how_clicked(self):
        self.hide()
        self.how_new=how_window()
        self.how_new.show()

    def addQuestion_clicked(self):
        self.hide()
        self.add=add_question()
        self.add.show()

    def exit_clicked(self):
        exit()


class how_window(QWidget):
    def __init__(self):
        super(how_window, self).__init__()
        self.main_ui()


    def main_ui(self):
        self.setGeometry(400,150,1200,800)
        self.setWindowTitle("QUIZ PCAP")
        self.setWindowIcon(QtGui.QIcon('logo.png'))



        self.titleText=QLabel('1.QUIZ zawiera 10 pytań\n2.Jeżeli użytkownik doda pytanie jest to pytanie bonusowe\n3.Każde pytanie posiada 4 odpowiedzi\n4.Nie ma ograniczenia czasowego na pytanie\n5.Jeżeli pytanie zostanie jest pytanie bonusowym\n6.Nie może byc wiecej niz 15 pytan\n7.Udanej Zabawy!',self)
        self.titleText.move(300,220)
        self.titleText.setFont(QFont('Times',25))
        self.titleText.adjustSize()


        self.return_button=QPushButton("Powrót",self)
        self.return_button.resize(150,35)
        self.return_button.setToolTip("Wraca do menu")
        self.return_button.clicked.connect(self.back_button)


        self.exit_button2=QPushButton("Wyjdź",self)
        self.exit_button2.resize(150,35)
        self.exit_button2.move(1050,0)
        self.exit_button2.setToolTip("Wylacza program")
        self.exit_button2.clicked.connect(self.exit_clicked2)


    def back_button(self):
        self.hide()
        self.ret=main_window()
        self.ret.show()

    def exit_clicked2(self):
        exit()

class add_question(QWidget):
    def __init__(self):
        super(add_question, self).__init__()
        self.main_ui()

    def main_ui(self):
        self.setGeometry(550,200,1000,600)
        self.setWindowTitle("QUIZ PCAP")
        self.setWindowIcon(QtGui.QIcon('logo.png'))

        self.nameQuestion=QLabel(self)
        self.nameQuestion.setText("Treść pytania: ")
        self.line1=QLineEdit(self)

        self.line1.move(400, 80)
        self.line1.resize(200, 30)
        self.nameQuestion.move(400, 50)


        self.answerA=QLabel(self)
        self.answerA.setText("Odpowiedź A: ")
        self.line2=QLineEdit(self)

        self.line2.move(400,150)
        self.line2.resize(200,30)
        self.answerA.move(400,120)

        self.answerB=QLabel(self)
        self.answerB.setText("Odpowiedź B: ")
        self.line3=QLineEdit(self)

        self.line3.move(400,220)
        self.line3.resize(200,30)
        self.answerB.move(400,190)


        self.answerC=QLabel(self)
        self.answerC.setText("Odpowiedź C: ")
        self.line4=QLineEdit(self)

        self.line4.move(400,290)
        self.line4.resize(200,30)
        self.answerC.move(400,260)

        self.answerD=QLabel(self)
        self.answerD.setText("Odpowiedź D: ")
        self.line5=QLineEdit(self)

        self.line5.move(400,360)
        self.line5.resize(200,30)
        self.answerD.move(400,330)

        self.add_button=QPushButton("Dodaj",self)
        self.add_button.resize(150,35)
        self.add_button.setToolTip("Dodaje pytanie bonusowe")
        self.add_button.move(420,500)
        self.add_button.clicked.connect(self.add_clicked)


        self.return_button=QPushButton("Powrót",self)
        self.return_button.resize(150,35)
        self.return_button.setToolTip("Wraca do menu")
        self.return_button.clicked.connect(self.back_button)
    def add_clicked(self):
        self.answers=["A","B","C","D"]
        self.answers,self.done=QtWidgets.QInputDialog.getItem(self,"QUIZ PCAP","Poprawna odpowiedź",self.answers)
        if self.done:
            self.hide()
            self.back_win=main_window()
            self.back_win.bonusQuestion=True
            self.back_win.selectGoodAnswer=self.answers
            self.back_win.dict_bonus={"question":self.line1.text(),"answerA":self.line2.text(),"answerB":self.line3.text(),"answerC":self.line4.text(),"answerD":self.line5.text()}
            self.back_win.show()

    def back_button(self):
        self.hide()
        self.ret=main_window()
        self.ret.show()



def main():
    if __name__ =="__main__":
        try:
            app=QApplication(sys.argv)
            win=main_window()
            win.show()
            sys.exit(app.exec())
        except:
            print("Program Error")

main()

