from PyQt5 import QtWidgets
from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import pyqtSlot
import sys
from PyQt5.QtWidgets import (QApplication, QWidget, QRadioButton,QPushButton, QLabel, QLineEdit, QGridLayout, QMessageBox,QInputDialog,QVBoxLayout)
from MainQuiz import *
from datetime import date,datetime

class bonus_question(QWidget):


    def __init__(self,UserText,dict_bonus,score,selectGoodAnswer):
        super(bonus_question, self).__init__()
        self.dict_bonus=dict_bonus
        self.UserText=UserText
        self.score=score
        self.int=0
        self.selectGoodAnswer=selectGoodAnswer
        self.quiz_ui()

    def quiz_ui(self):
        self.setGeometry(400,150,1200,800)
        self.setWindowTitle("QUIZ PCAP")
        self.setWindowIcon(QtGui.QIcon('logo.png'))

        self.questionNumberText=QLabel("BONUS",self)
        self.questionNumberText.move(0,0)
        self.questionNumberText.setFont(QFont("Times",20))
        self.questionNumberText.adjustSize()

        self.titleText=QLabel(self.dict_bonus["question"],self)
        self.titleText.move(420,100)
        self.titleText.setFont(QFont('Times',25))
        self.titleText.adjustSize()

        self.radioButtonA=QRadioButton(self.dict_bonus["answerA"],self)
        self.radioButtonA.move(520,500)
        self.radioButtonA.clicked.connect(self.check)

        self.radioButtonB=QRadioButton(self.dict_bonus["answerB"],self)
        self.radioButtonB.move(520,520)
        self.radioButtonB.clicked.connect(self.check)

        self.radioButtonC=QRadioButton(self.dict_bonus["answerC"],self)
        self.radioButtonC.move(520,540)
        self.radioButtonC.clicked.connect(self.check)

        self.radioButtonD=QRadioButton(self.dict_bonus["answerD"],self)
        self.radioButtonD.move(520,560)
        self.radioButtonD.clicked.connect(self.check)

        self.nextQuestionButton=QPushButton("Nastepne pytanie",self)
        self.nextQuestionButton.resize(150,35)
        self.nextQuestionButton.setToolTip("Przechodzi do nastepnego pytania")
        self.nextQuestionButton.clicked.connect(self.go_next)
        self.nextQuestionButton.move(500,650)

    def check(self):
        if self.selectGoodAnswer=='A':
            if self.radioButtonA.isChecked():
                self.int+=1
            else:
                self.int=0
        elif self.selectGoodAnswer=="B":
            if self.radioButtonB.isChecked():
                self.int+=1
            else:
                self.int=0
        elif self.selectGoodAnswer=="C":
            if self.radioButtonC.isChecked():
                self.int+=1
            else:
                self.int=0
        elif self.selectGoodAnswer=="D":
            if self.radioButtonD.isChecked():
                self.int+=1
            else:
                self.int=0

    def go_next(self):
        self.score+=self.int
        self.hide()
        self.openNewQuestion=quiz_windowQ1(self.UserText,self.score)
        self.openNewQuestion.show()

class quiz_windowQ1(QWidget):


    def __init__(self,UserText,score):
        super(quiz_windowQ1, self).__init__()
        self.UserText=UserText
        self.score=score
        self.int=0
        self.quiz_ui()

    def quiz_ui(self):
        self.setGeometry(400,150,1200,800)
        self.setWindowTitle("QUIZ PCAP")
        self.setWindowIcon(QtGui.QIcon('logo.png'))

        self.questionNumberText=QLabel("1/10",self)
        self.questionNumberText.move(0,0)
        self.questionNumberText.setFont(QFont("Times",20))
        self.questionNumberText.adjustSize()

        self.titleText=QLabel('Co wyświetli program?\n\na=[1,2,3]\nb=a\nb[1]=1\nprint(a)',self)
        self.titleText.move(420,100)
        self.titleText.setFont(QFont('Times',25))
        self.titleText.adjustSize()

        self.radioButtonA=QRadioButton("1,2,3",self)
        self.radioButtonA.move(520,500)
        self.radioButtonA.clicked.connect(self.check)

        self.radioButtonB=QRadioButton("błąd",self)
        self.radioButtonB.move(520,520)
        self.radioButtonB.clicked.connect(self.check)

        self.radioButtonC=QRadioButton("3,2,1",self)
        self.radioButtonC.move(520,540)
        self.radioButtonC.clicked.connect(self.check)


        self.radioButtonD=QRadioButton("1,1,3",self)   #Dobre
        self.radioButtonD.move(520,560)
        self.radioButtonD.clicked.connect(self.check)

        self.nextQuestionButton=QPushButton("Nastepne pytanie",self)
        self.nextQuestionButton.resize(150,35)
        self.nextQuestionButton.setToolTip("Przechodzi do nastepnego pytania")
        self.nextQuestionButton.clicked.connect(self.go_next)
        self.nextQuestionButton.move(500,650)

    def check(self):
        if self.radioButtonD.isChecked():
            self.int+=1
        else:
            self.int=0
    def go_next(self):
        self.score+=self.int
        self.hide()
        self.openNewQuestion=quiz_windowQ2(self.UserText,self.score)
        self.openNewQuestion.show()


class quiz_windowQ2(QWidget):
    def __init__(self,UserText,score):
        super(quiz_windowQ2, self).__init__()
        self.UserText=UserText
        self.score=score
        self.int=0
        self.quiz_ui()

    def quiz_ui(self):
        self.setGeometry(400,150,1200,800)
        self.setWindowTitle("QUIZ PCAP")
        self.setWindowIcon(QtGui.QIcon('logo.png'))

        self.questionNumberText=QLabel("2/10",self)
        self.questionNumberText.move(0,0)
        self.questionNumberText.setFont(QFont("Times",20))
        self.questionNumberText.adjustSize()

        self.titleText=QLabel('Co należy wpisać w miejsce XXX \naby został wyprintowany moduł name?\n\nIf __name__ != "XXX":\n\tprint(__name__)',self)
        self.titleText.move(420,100)
        self.titleText.setFont(QFont('Times',25))
        self.titleText.adjustSize()

        self.radioButtonA=QRadioButton("main",self)
        self.radioButtonA.move(520,500)
        self.radioButtonA.clicked.connect(self.check)

        self.radioButtonB=QRadioButton("_main_",self)
        self.radioButtonB.move(520,520)
        self.radioButtonB.clicked.connect(self.check)

        self.radioButtonC=QRadioButton("__main__",self) #Dobre
        self.radioButtonC.move(520,540)
        self.radioButtonC.clicked.connect(self.check)

        self.radioButtonD=QRadioButton("___main___",self)
        self.radioButtonD.move(520,560)
        self.radioButtonD.clicked.connect(self.check)

        self.nextQuestionButton=QPushButton("Nastepne pytanie",self)
        self.nextQuestionButton.resize(150,35)
        self.nextQuestionButton.setToolTip("Przechodzi do nastepnego pytania")
        self.nextQuestionButton.move(500,650)
        self.nextQuestionButton.clicked.connect(self.go_next)

    def check(self):
        if self.radioButtonC.isChecked():
            self.int+=1
        else:
            self.int=0

    def go_next(self):
        self.score+=self.int
        self.hide()
        self.openNewQuestion=quiz_windowQ3(self.UserText,self.score)
        self.openNewQuestion.show()


class quiz_windowQ3(QWidget):
    def __init__(self,UserText,score):
        super(quiz_windowQ3, self).__init__()
        self.UserText=UserText
        self.score=score
        self.int=0
        self.quiz_ui()

    def quiz_ui(self):
        self.setGeometry(400,150,1200,800)
        self.setWindowTitle("QUIZ PCAP")
        self.setWindowIcon(QtGui.QIcon('logo.png'))

        self.questionNumberText=QLabel("3/10",self)
        self.questionNumberText.move(0,0)
        self.questionNumberText.setFont(QFont("Times",20))
        self.questionNumberText.adjustSize()

        self.titleText=QLabel('Które równanie jest poprawne(Zaznacz 2 odpowiedzi)',self)
        self.titleText.move(320,100)
        self.titleText.setFont(QFont('Times',25))
        self.titleText.adjustSize()

        self.checkBoxA=QCheckBox("chr (ord (x)) = = x",self)    #Dobre
        self.checkBoxA.move(520,500)
        self.checkBoxA.stateChanged.connect(self.check)

        self.checkBoxB=QCheckBox("ord (ord (x)) = = x",self)
        self.checkBoxB.move(520,520)
        self.checkBoxB.stateChanged.connect(self.check)

        self.checkBoxC=QCheckBox("chr (chr (x)) = = x",self)
        self.checkBoxC.move(520,540)
        self.checkBoxC.stateChanged.connect(self.check)

        self.checkBoxD=QCheckBox("ord (chr (x)) = = x",self)    #Dobre
        self.checkBoxD.move(520,560)
        self.checkBoxD.stateChanged.connect(self.check)

        self.nextQuestionButton=QPushButton("Nastepne pytanie",self)
        self.nextQuestionButton.resize(150,35)
        self.nextQuestionButton.setToolTip("Przechodzi do nastepnego pytania")
        self.nextQuestionButton.move(500,650)
        self.nextQuestionButton.clicked.connect(self.go_next)

    def check(self):
        if self.checkBoxD.isChecked() and self.checkBoxA.isChecked():
            self.int+=2
        else:
            self.int=0

    def go_next(self):
        self.score+=self.int
        self.hide()
        self.openNewQuestion=quiz_windowQ4(self.UserText,self.score)
        self.openNewQuestion.show()

class quiz_windowQ4(QWidget):
    def __init__(self,UserText,score):
        super(quiz_windowQ4, self).__init__()
        self.UserText=UserText
        self.score=score
        self.int=0
        self.quiz_ui()

    def quiz_ui(self):
        self.setGeometry(400,150,1200,800)
        self.setWindowTitle("QUIZ PCAP")
        self.setWindowIcon(QtGui.QIcon('logo.png'))

        self.questionNumberText=QLabel("4/10",self)
        self.questionNumberText.move(0,0)
        self.questionNumberText.setFont(QFont("Times",20))
        self.questionNumberText.adjustSize()

        self.titleText=QLabel('Które operatory służa to przesunięć bitowych(Zaznacz 2 odpowiedzi)',self)
        self.titleText.move(150,100)
        self.titleText.setFont(QFont('Times',25))
        self.titleText.adjustSize()

        self.checkBoxA=QCheckBox(">>",self)    #Dobre
        self.checkBoxA.move(520,500)
        self.checkBoxA.stateChanged.connect(self.check)

        self.checkBoxB=QCheckBox("--",self)
        self.checkBoxB.move(520,520)
        self.checkBoxB.stateChanged.connect(self.check)

        self.checkBoxC=QCheckBox("<<",self) #Dobre
        self.checkBoxC.move(520,540)
        self.checkBoxC.stateChanged.connect(self.check)

        self.checkBoxD=QCheckBox("++",self)
        self.checkBoxD.move(520,560)
        self.checkBoxD.stateChanged.connect(self.check)

        self.nextQuestionButton=QPushButton("Nastepne pytanie",self)
        self.nextQuestionButton.resize(150,35)
        self.nextQuestionButton.setToolTip("Przechodzi do nastepnego pytania")
        self.nextQuestionButton.move(500,650)
        self.nextQuestionButton.clicked.connect(self.go_next)


    def check(self):
        if self.checkBoxA.isChecked() and self.checkBoxC.isChecked():
            self.int+=2
        else:
            self.int=0

    def go_next(self):
        self.score+=self.int
        self.hide()
        self.openNewQuestion=quiz_windowQ5(self.UserText,self.score)
        self.openNewQuestion.show()

class quiz_windowQ5(QWidget):
    def __init__(self,UserText,score):
        super(quiz_windowQ5, self).__init__()
        self.UserText=UserText
        self.score=score
        self.int=0
        self.quiz_ui()

    def quiz_ui(self):
        self.setGeometry(400,150,1200,800)
        self.setWindowTitle("QUIZ PCAP")
        self.setWindowIcon(QtGui.QIcon('logo.png'))

        self.questionNumberText=QLabel("5/10",self)
        self.questionNumberText.move(0,0)
        self.questionNumberText.setFont(QFont("Times",20))
        self.questionNumberText.adjustSize()

        self.titleText=QLabel('Co wyświetli skrypt?\n\nclass A:\n\tdef __init__(self,a=2:\n\t\tself.a=a\n\na=A()\nprint(a.a)',self)
        self.titleText.move(420,100)
        self.titleText.setFont(QFont('Times',25))
        self.titleText.adjustSize()

        self.radioButtonA=QRadioButton("2",self)    #Dobre
        self.radioButtonA.move(520,500)
        self.radioButtonA.clicked.connect(self.check)

        self.radioButtonB=QRadioButton("błąd",self)
        self.radioButtonB.move(520,520)
        self.radioButtonB.clicked.connect(self.check)

        self.radioButtonC=QRadioButton("None",self)
        self.radioButtonC.move(520,540)
        self.radioButtonC.clicked.connect(self.check)

        self.radioButtonD=QRadioButton("Pusta linia",self)
        self.radioButtonD.move(520,560)
        self.radioButtonD.clicked.connect(self.check)

        self.nextQuestionButton=QPushButton("Nastepne pytanie",self)
        self.nextQuestionButton.resize(150,35)
        self.nextQuestionButton.setToolTip("Przechodzi do nastepnego pytania")
        self.nextQuestionButton.move(500,650)
        self.nextQuestionButton.clicked.connect(self.go_next)

    def check(self):
        if self.radioButtonA.isChecked():
            self.int+=1
        else:
            self.int=0

    def go_next(self):
        self.score+=self.int
        self.hide()
        self.openNewQuestion=quiz_windowQ6(self.UserText,self.score)
        self.openNewQuestion.show()

class quiz_windowQ6(QWidget):
    def __init__(self,UserText,score):
        super(quiz_windowQ6, self).__init__()
        self.UserText=UserText
        self.score=score
        self.int=0
        self.quiz_ui()

    def quiz_ui(self):
        self.setGeometry(400,150,1200,800)
        self.setWindowTitle("QUIZ PCAP")
        self.setWindowIcon(QtGui.QIcon('logo.png'))

        self.questionNumberText=QLabel("6/10",self)
        self.questionNumberText.move(0,0)
        self.questionNumberText.setFont(QFont("Times",20))
        self.questionNumberText.adjustSize()

        self.titleText=QLabel('Co wyświetli skrypt?\n\nclass Top:\n\tdef m_top(self):\n\t\tprint("top")\n\nprint(Top.__bases__)',self)
        self.titleText.move(420,100)
        self.titleText.setFont(QFont('Times',25))
        self.titleText.adjustSize()

        self.radioButtonA=QRadioButton("None",self)
        self.radioButtonA.move(520,500)
        self.radioButtonA.clicked.connect(self.check)

        self.radioButtonB=QRadioButton("błąd",self)
        self.radioButtonB.move(520,520)
        self.radioButtonB.clicked.connect(self.check)

        self.radioButtonC=QRadioButton("(<class 'object'>,)",self)  #Dobre
        self.radioButtonC.move(520,540)
        self.radioButtonC.clicked.connect(self.check)

        self.radioButtonD=QRadioButton("bases Top",self)
        self.radioButtonD.move(520,560)
        self.radioButtonD.clicked.connect(self.check)

        self.nextQuestionButton=QPushButton("Nastepne pytanie",self)
        self.nextQuestionButton.resize(150,35)
        self.nextQuestionButton.setToolTip("Przechodzi do nastepnego pytania")
        self.nextQuestionButton.move(500,650)
        self.nextQuestionButton.clicked.connect(self.go_next)


    def check(self):
        if self.radioButtonC.isChecked():
            self.int+=1
        else:
            self.int=0

    def go_next(self):
        self.score+=self.int
        self.hide()
        self.openNewQuestion=quiz_windowQ7(self.UserText,self.score)
        self.openNewQuestion.show()

class quiz_windowQ7(QWidget):
    def __init__(self,UserText,score):
        super(quiz_windowQ7, self).__init__()
        self.UserText=UserText
        self.score=score
        self.int=0
        self.quiz_ui()

    def quiz_ui(self):
        self.setGeometry(400,150,1200,800)
        self.setWindowTitle("QUIZ PCAP")
        self.setWindowIcon(QtGui.QIcon('logo.png'))

        self.questionNumberText=QLabel("7/10",self)
        self.questionNumberText.move(0,0)
        self.questionNumberText.setFont(QFont("Times",20))
        self.questionNumberText.adjustSize()

        self.titleText=QLabel('Co wyświetli skrypt?\n\nclass Language:\n\tpass\n\npython=Language().__init__() \nprint(type(python))',self)
        self.titleText.move(420,100)
        self.titleText.setFont(QFont('Times',25))
        self.titleText.adjustSize()

        self.radioButtonA=QRadioButton("Language",self)
        self.radioButtonA.move(520,500)
        self.radioButtonA.clicked.connect(self.check)

        self.radioButtonB=QRadioButton("<class 'NoneType'>",self)   #Dobre
        self.radioButtonB.move(520,520)
        self.radioButtonB.clicked.connect(self.check)

        self.radioButtonC=QRadioButton("<class '__main__.Language'>",self)
        self.radioButtonC.move(520,540)
        self.radioButtonC.clicked.connect(self.check)

        self.radioButtonD=QRadioButton("błąd",self)
        self.radioButtonD.move(520,560)
        self.radioButtonD.clicked.connect(self.check)

        self.nextQuestionButton=QPushButton("Nastepne pytanie",self)
        self.nextQuestionButton.resize(150,35)
        self.nextQuestionButton.setToolTip("Przechodzi do nastepnego pytania")
        self.nextQuestionButton.move(500,650)
        self.nextQuestionButton.clicked.connect(self.go_next)

    def check(self):
        if self.radioButtonB.isChecked():
            self.int+=1
        else:
            self.int=0

    def go_next(self):
        self.score+=self.int
        self.hide()
        self.openNewQuestion=quiz_windowQ8(self.UserText,self.score)
        self.openNewQuestion.show()

class quiz_windowQ8(QWidget):
    def __init__(self,UserText,score):
        super(quiz_windowQ8, self).__init__()
        self.UserText=UserText
        self.score=score
        self.int=0
        self.quiz_ui()

    def quiz_ui(self):
        self.setGeometry(400,150,1200,800)
        self.setWindowTitle("QUIZ PCAP")
        self.setWindowIcon(QtGui.QIcon('logo.png'))

        self.questionNumberText=QLabel("8/10",self)
        self.questionNumberText.move(0,0)
        self.questionNumberText.setFont(QFont("Times",20))
        self.questionNumberText.adjustSize()

        self.titleText=QLabel('Co wyświetli skrypt?\n\na=[1,2,3]\nb=a\nc=a[:]\nd=a.copy()\nprint(b is a, c is a, d is a)',self)
        self.titleText.move(420,100)
        self.titleText.setFont(QFont('Times',25))
        self.titleText.adjustSize()

        self.radioButtonA=QRadioButton("False True True",self)
        self.radioButtonA.move(520,500)
        self.radioButtonA.clicked.connect(self.check)

        self.radioButtonB=QRadioButton("True False False",self)     #Dobre
        self.radioButtonB.move(520,520)
        self.radioButtonB.clicked.connect(self.check)

        self.radioButtonC=QRadioButton("bład",self)
        self.radioButtonC.move(520,540)
        self.radioButtonC.clicked.connect(self.check)

        self.radioButtonD=QRadioButton("False False False",self)
        self.radioButtonD.move(520,560)
        self.radioButtonD.clicked.connect(self.check)

        self.nextQuestionButton=QPushButton("Nastepne pytanie",self)
        self.nextQuestionButton.resize(150,35)
        self.nextQuestionButton.setToolTip("Przechodzi do nastepnego pytania")
        self.nextQuestionButton.move(500,650)
        self.nextQuestionButton.clicked.connect(self.go_next)


    def check(self):
        if self.radioButtonB.isChecked():
            self.int+=1
        else:
            self.int=0


    def go_next(self):
        self.score+=self.int
        self.hide()
        self.openNewQuestion=quiz_windowQ9(self.UserText,self.score)
        self.openNewQuestion.show()

class quiz_windowQ9(QWidget):
    def __init__(self,UserText,score):
        super(quiz_windowQ9, self).__init__()
        self.UserText=UserText
        self.score=score
        self.int=0
        self.quiz_ui()

    def quiz_ui(self):
        self.setGeometry(400,150,1200,800)
        self.setWindowTitle("QUIZ PCAP")
        self.setWindowIcon(QtGui.QIcon('logo.png'))

        self.questionNumberText=QLabel("9/10",self)
        self.questionNumberText.move(0,0)
        self.questionNumberText.setFont(QFont("Times",20))
        self.questionNumberText.adjustSize()

        self.titleText=QLabel('Co wyświetli skrypt?\n\nx="axbcd"\nprint(sorted(x))',self)
        self.titleText.move(420,100)
        self.titleText.setFont(QFont('Times',25))
        self.titleText.adjustSize()

        self.radioButtonA=QRadioButton("błąd",self)
        self.radioButtonA.move(520,500)
        self.radioButtonA.clicked.connect(self.check)

        self.radioButtonB=QRadioButton("'abcdx'",self)
        self.radioButtonB.move(520,520)
        self.radioButtonB.clicked.connect(self.check)

        self.radioButtonC=QRadioButton("'axbcd'",self)
        self.radioButtonC.move(520,540)
        self.radioButtonC.clicked.connect(self.check)

        self.radioButtonD=QRadioButton("['a','b','c','d','x']",self)   #Dobre
        self.radioButtonD.move(520,560)
        self.radioButtonD.clicked.connect(self.check)

        self.nextQuestionButton=QPushButton("Nastepne pytanie",self)
        self.nextQuestionButton.resize(150,35)
        self.nextQuestionButton.setToolTip("Przechodzi do nastepnego pytania")
        self.nextQuestionButton.move(500,650)
        self.nextQuestionButton.clicked.connect(self.go_next)


    def check(self):
        if self.radioButtonD.isChecked():
            self.int+=1
        else:
            self.int=0


    def go_next(self):
        self.score+=self.int
        self.hide()
        self.openNewQuestion=quiz_windowQ10(self.UserText,self.score)
        self.openNewQuestion.show()

class quiz_windowQ10(QWidget):
    def __init__(self,UserText,score):
        super(quiz_windowQ10, self).__init__()
        self.UserText=UserText
        self.score=score
        self.int=0
        self.quiz_ui()

    def quiz_ui(self):
        self.setGeometry(400,150,1200,800)
        self.setWindowTitle("QUIZ PCAP")
        self.setWindowIcon(QtGui.QIcon('logo.png'))

        self.questionNumberText=QLabel("10/10",self)
        self.questionNumberText.move(0,0)
        self.questionNumberText.setFont(QFont("Times",20))
        self.questionNumberText.adjustSize()

        self.titleText=QLabel('Co wyświetli skrypt\n\nfor i in range(0,3):\n\tpass\nprint(i)',self)
        self.titleText.move(420,100)
        self.titleText.setFont(QFont('Times',25))
        self.titleText.adjustSize()

        self.radioButtonA=QRadioButton("0",self)
        self.radioButtonA.move(520,500)
        self.radioButtonA.clicked.connect(self.check)

        self.radioButtonB=QRadioButton("2",self)    #Dobre
        self.radioButtonB.move(520,520)
        self.radioButtonB.clicked.connect(self.check)

        self.radioButtonC=QRadioButton("3",self)
        self.radioButtonC.move(520,540)
        self.radioButtonC.clicked.connect(self.check)

        self.radioButtonD=QRadioButton("błąd",self)
        self.radioButtonD.move(520,560)
        self.radioButtonD.clicked.connect(self.check)

        self.nextQuestionButton=QPushButton("Nastepne pytanie",self)
        self.nextQuestionButton.resize(150,35)
        self.nextQuestionButton.setToolTip("Przechodzi do nastepnego pytania")
        self.nextQuestionButton.move(500,650)
        self.nextQuestionButton.clicked.connect(self.go_next)

    def check(self):
        if self.radioButtonB.isChecked():
            self.int+=1
        else:
            self.int=0


    def go_next(self):
        self.score+=self.int
        self.hide()
        self.open_new=quiz_endingWindow(self.UserText,self.score)
        self.open_new.show()

class quiz_endingWindow(QWidget):
    def __init__(self,UserText,score):
        super(quiz_endingWindow, self).__init__()
        self.UserText=UserText
        self.score=score
        self.today=date.today()
        self.quiz_ui()

    def quiz_ui(self,):
        self.setGeometry(400,150,1200,800)
        self.setWindowTitle("QUIZ PCAP")
        self.setWindowIcon(QtGui.QIcon('logo.png'))


        self.titleText=QLabel(f"Gratulacje {self.UserText}!\nQUIZ został ukończony\nTwój wynik to:\n{self.score}/12 punktów",self)


        self.file=open("Wyniki_Graczy.txt","a")
        self.file.write(f"Data:{self.today} Gracz:{self.UserText} Wynik:{self.score}/12\n")
        self.file.close()


        self.titleText.move(420,100)
        self.titleText.setFont(QFont('Times',30))
        self.titleText.adjustSize()

        self.again_button=QPushButton("Zagraj ponownie",self)
        self.again_button.resize(150,35)
        self.again_button.move(530,400)
        self.again_button.setToolTip("Rozpoczyna  ponownie quiz QUIZ")
        self.again_button.clicked.connect(self.again_clicked)


        self.menu_button=QPushButton("Menu",self)
        self.menu_button.resize(150,35)
        self.menu_button.setToolTip("Wraca do menu")
        self.menu_button.move(530,450)
        self.menu_button.clicked.connect(self.menu_clicked)


        self.exit_button=QPushButton("Wyjdź",self)
        self.exit_button.resize(150,35)
        self.exit_button.move(530,500)
        self.exit_button.setToolTip("Wyłącza aplikacje")
        self.exit_button.clicked.connect(self.exit_clicked)


    def again_clicked(self):
        self.text,self.ok=QInputDialog.getText(self,'QUIZ PCAP','Podaj nick:')
        if self.ok:
            self.score=0
            self.hide()
            self.open_window=quiz_windowQ1(self.UserText,self.score)
            self.open_window.show()

    def menu_clicked(self):
        self.hide()
        self.menu_window=main_window()
        self.menu_window.show()

    def exit_clicked(self):
        exit()
